//  CollectionViewCell.swift
//  BuzCard

//  Created by appentus technologies pvt. ltd. on 8/3/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit


enum Color_Type_PP {
    case red
    case green
    case gray
}

extension Color_Type_PP: RawRepresentable {
    typealias RawValue = UIColor
    
    init?(rawValue: RawValue) {
        switch rawValue {
            case #colorLiteral(red: 0.7058823529, green: 0, blue: 0, alpha: 1): self = .red
            case #colorLiteral(red: 0.3725490196, green: 0.568627451, blue: 0.1843137255, alpha: 1): self = .green
            case #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1): self = .gray
            default: return nil
        }
    }
    
    var rawValue: RawValue {
        switch self {
            case .red: return #colorLiteral(red: 0.7058823529, green: 0, blue: 0, alpha: 1)
            case .green: return #colorLiteral(red: 0.3725490196, green: 0.568627451, blue: 0.1843137255, alpha: 1)
            case .gray: return #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
        }
    }
}




