//
//  MoveCell.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/20/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class MoveCell: UITableViewCell {

    @IBOutlet weak var selectUnselectBttn: UIButton!
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var groupDescriptionLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
