//
//  contact_list_tableCell.swift
//  BuzCard
//
//  Created by appentus on 8/10/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class contact_list_tableCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var leftSwipeView: UIView!
//    @IBOutlet weak var leftSwipeViewWidth: NSLayoutConstraint!
    @IBOutlet weak var deleteBttn: UIButton!
    @IBOutlet weak var editBttn: UIButton!
    @IBOutlet weak var view_width: NSLayoutConstraint!
    @IBOutlet weak var check_box_btn: UIButton!
    @IBOutlet weak var profile_leading: NSLayoutConstraint!
    @IBOutlet weak var select_BTN: UIButton!
    @IBOutlet weak var profile_select: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profile_leading.constant = 20
        view_width.constant  = 0
        check_box_btn.isHidden = true
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
