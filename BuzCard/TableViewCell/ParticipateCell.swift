//
//  ParticipateCell.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/20/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class ParticipateCell: UITableViewCell {

    @IBOutlet weak var cancelBttn: UIButton!
    @IBOutlet weak var participateImg: UIImageView!
    @IBOutlet weak var participateName: UILabel!
    @IBOutlet weak var bottomView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
