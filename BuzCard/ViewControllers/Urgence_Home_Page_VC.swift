//  CollectionViewCell.swift
//  BuzCard

//  Created by appentus technologies pvt. ltd. on 8/3/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit
import SideMenuSwift


class Urgence_Home_Page_VC: UIViewController , Loading_PP_Delegate,SideMenuControllerDelegate {
    
    var loading_PP_VC:Loading_PP_VC!
    
       @IBOutlet weak var menuBttn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
      
        menuBttn.action = #selector(menuButtonTapped(sender:))
        func_set_UI()
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }
    func func_set_UI() {
        let storyBoard = UIStoryboard  (name: "POP-UP", bundle: nil)
        loading_PP_VC = storyBoard.instantiateViewController(withIdentifier: "Loading_PP_VC") as? Loading_PP_VC
        loading_PP_VC.delegate = self
    }
    
    @IBAction func profileBttn(_ sender: UIButton) {
        
       func_PUSH_VC("NewProfileVC")
    }
    func func_add_Loading_VC() {
        self.addChild(loading_PP_VC)
        self.view.addSubview(loading_PP_VC.view)
    }
    
    @IBAction func btn_Level_Emergency(_ sender:UIButton) {
        func_add_Loading_VC()
        
    }
    
    @IBAction func btn_Speaker(_ sender:UIButton) {
        func_add_Loading_VC()
    }
    
    @IBAction func btn_SMS(_ sender:UIButton) {
        func_add_Loading_VC()
    }
    
    @IBAction func btn_QR_Code(_ sender:UIButton) {
        func_add_Loading_VC()
    }
    
    func func_OK() {
        func_PUSH_VC("EmailContactVC")
    }
    
    func func_Annuler() {
        print("Func annuler is calling")
        
    }
    
    @IBAction func business_VC(_ sender: Any) {
        
        func_PUSH_VC("Home_Page_VC")
    }
}
