//  Home_Page_VC.swift
//  BuzCard

//  Created by appentus technologies pvt. ltd. on 8/3/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit
import SideMenuSwift


class Home_Page_VC: UIViewController,SideMenuControllerDelegate, UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    @IBOutlet weak var homeCollection: UICollectionView!
    @IBOutlet weak var page_control:UIPageControl!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
  //  @IBOutlet weak var menuBttn: UIBarButtonItem!
     @IBOutlet weak var pageControllerTopConstraint: NSLayoutConstraint!
   
    var arr_home_data = [["title":"Nouveau profil","email":"loginmail@mail.com","name":"Actualiser","image":"dotted-line","Plus":"+"],
                        ["title":"Profil","email":"w.smith@gmail.com","name":"creer","image":"smith","Plus":""]]
    
    var val = false
    override func viewDidLoad() {
        super.viewDidLoad()
        homeCollection.delegate = self
        homeCollection.dataSource = self
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
        page_control.numberOfPages = arr_home_data.count
        val = UserDefaults.standard.object(forKey: "once") as? Bool ?? false
        if val == false{
            UserDefaults.standard.set(true, forKey: "once")
            NotificationCenter.default.addObserver(self, selector: #selector(self.side_menu_actions(sender:)), name: Notification.Name("side_menu_actions"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.ok_Action(sender:)), name: Notification.Name("ok_actions"), object: nil)
        }
      //  menuBttn.action = #selector(menuButtonTapped(sender:))
    }
    @objc func side_menu_actions( sender : NSNotification){
        let sender = sender.userInfo as! [String:Any]
        let sender_tag = sender["tag"] as? Int ?? 0
        switch  sender_tag{
        case 11:
              sideMenuController?.hideMenu()
            let s                   = UIScreen.main.bounds.size
            let pv                  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuLanguageVC") as! MenuLanguageVC
            pv.view.frame           = CGRect(x:0, y:0, width: s.width - 40, height: 180)
            pv.view.backgroundColor = .black
            popUpEffectType         = .flipDown //.zoomIn(default)/.zoomOut/.flipUp/.flipDown
            presentPopUpViewController(pv)
            
//            let storyBoard = UIStoryboard  (name: "POP-UP", bundle: nil)
//            loading_PP_VC = storyBoard.instantiateViewController(withIdentifier: "Loading_PP_VC") as? Loading_PP_VC
//            loading_PP_VC.delegate = self
////            self.addChild(loading_PP_VC)
////            self.view.addSubview(loading_PP_VC.view)
//            UIApplication.shared.keyWindow?.rootViewController!.addChild(loading_PP_VC)
//            UIApplication.shared.keyWindow?.rootViewController!.view.addSubview(loading_PP_VC.view)
//
            
        case 12:
              sideMenuController?.hideMenu()
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login_VC") as? Login_VC
            self.navigationController?.pushViewController(vc!, animated: true)
            
           
                 // func_PUSH_VC("Login_VC")
            
        case 5:
             sideMenuController?.hideMenu()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ToutrialVC") as? ToutrialVC
        self.navigationController?.pushViewController(vc!, animated: true)
          
           
        case 3:
              sideMenuController?.hideMenu()
            if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QRCode2VC") as? QRCode2VC {
             self.navigationController?.pushViewController(vc, animated: true)
            }
        case 7:
              sideMenuController?.hideMenu()
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ParaniageVC") as? ParaniageVC
            self.navigationController?.pushViewController(vc!, animated: true)
         //   func_PUSH_VC("ParaniageVC")
//             func_PUSH_VC("MenuLanguageVC")
        case 8:
              sideMenuController?.hideMenu()
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConfidentialVC") as? ConfidentialVC
            self.navigationController?.pushViewController(vc!, animated: true)
           // func_PUSH_VC("ConfidentialVC")
        default:
            0
        }
        
    }
    
    
   @objc func ok_Action( sender : NSNotification){
         sideMenuController?.hideMenu()
         dismissPopUpViewController()
    
        let s                   = UIScreen.main.bounds.size
        let pv                  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeLoaderVC") as! HomeLoaderVC
        pv.view.frame           = CGRect(x:0, y:0, width: s.width - 40, height: 180)
//        pv.view.backgroundColor = .black
        popUpEffectType         = .flipDown //.zoomIn(default)/.zoomOut/.flipUp/.flipDown
        presentPopUpViewController(pv)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            self.dismissPopUpViewController()
        }
    }
    
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        sideMenuController?.revealMenu()
    }
    
    override func viewDidLayoutSubviews() {
        let screenBounds = UIScreen.main.bounds
        // let screen_width = screenBounds.width
        let screen_height = screenBounds.height
        
        print("Screen Height: \(screen_height)")
        if screen_height == 896.0 {
         
                pageControllerTopConstraint.constant = 31
           
            
          
        } else if screen_height == 812.0 {
           
                pageControllerTopConstraint.constant = 27
            
                
         
            
        } else if screen_height == 736.0 {
          
                pageControllerTopConstraint.constant = 26
          
            
        }
        else if screen_height == 667.0 {
          
                pageControllerTopConstraint.constant = 31
           
            
        }
        else if screen_height == 568.0 {
          
           
                pageControllerTopConstraint.constant = 25
           
            
        }
    }
    @IBAction func btn_urgence(_ sender: Any) {
        func_PUSH_VC("Urgence_Home_Page_VC")
    }
    @IBAction func btn_contact(_ sender: Any) {
        
        func_PUSH_VC("Contact_List_VC")

    }
    
    @IBAction func envoyer(_ sender: Any) {
        func_PUSH_VC("send_Info_VC")
    }
    
    @IBAction func scanner_btn(_ sender: Any) {
        func_PUSH_VC("contact_edit2_VC")

    }
    @objc func profile_btn(_ sender: UIButton){
        func_PUSH_VC("profile_info_VC")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width
        return CGSize (width: width, height: collectionView.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Home_Page_CollectionViewCell
        
        let dict = arr_home_data[indexPath.row]
        cell.lbl_title.text = dict["title"]
        cell.lbl_email.text = dict["email"]
        cell.lbl_name.text = dict["name"]
        
        cell.btn_Profile.setBackgroundImage(UIImage (named:dict["image"]!), for: .normal)
        cell.btn_Profile.setTitle(dict["Plus"], for: .normal)
        cell.btn_Profile.addTarget(self, action: #selector(profile_btn(_:)), for: .touchUpInside)
        cell.editProfileNameBttn.addTarget(self, action: #selector(editBttn(_:)), for: .touchUpInside)
        
        
        return cell
    }
    @objc func editBttn(_ sender: UIButton){
        func_PUSH_VC("profile_edit_VC")
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    
    @IBAction func btn_Left(_ sender: UIButton) {
        let collectionBounds = homeCollection.bounds
        let contentOffset = CGFloat(floor(homeCollection.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func btn_Right(_ sender: UIButton) {
        let collectionBounds = homeCollection.bounds
        let contentOffset = CGFloat(floor(homeCollection.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : homeCollection.contentOffset.y ,width : homeCollection.frame.width,height : homeCollection.frame.height)
        homeCollection.scrollRectToVisible(frame, animated: true)
    }
    
    

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2

        page_control.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }

    
}
