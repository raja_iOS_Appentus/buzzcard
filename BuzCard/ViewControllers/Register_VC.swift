
//  Register_VC.swift
//  BuzCard

//  Created by appentus technologies pvt. ltd. on 8/2/19.
//  Copyright © 2019 appentus. All rights reserved.




import UIKit



class Register_VC: UIViewController , SUCCESS_POPUP_Delegate {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_OK_Success), name: NSNotification.Name (rawValue: "success_OK"), object: nil)
    }
    
    @IBAction func btn_register(_ sender:UIButton) { 
        let storyboard = UIStoryboard (name: "POP-UP", bundle: nil)
        let success_POPUP = storyboard.instantiateViewController(withIdentifier: "Success_PP_VC") as! Success_PP_VC
        
        str_title = "Chargement dolor sit amet, consectetur adipiscing elit."
        
        self.addChild(success_POPUP)
        self.view.addSubview(success_POPUP.view)
        
        success_POPUP.delegate_Success = self
    }
    
    @objc func func_OK_Success() {
        func_PUSH_VC("SideMenuController")
    }
    
}



