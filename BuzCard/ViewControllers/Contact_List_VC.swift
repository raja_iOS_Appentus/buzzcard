//
//  Contact_List_VC.swift
//  BuzCard
//
//  Created by appentus technologies pvt. ltd. on 8/5/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift
import SwipeCellKit

class Contact_List_VC: UIViewController,SideMenuControllerDelegate {
    @IBOutlet weak var targetBttn: UIButton!
    @IBOutlet weak var tbl_contact_list:UITableView!
    @IBOutlet weak var tier_img: UIImageView!
    @IBOutlet weak var setting_img: UIImageView!
    @IBOutlet weak var option_lbl: UILabel!
    @IBOutlet weak var tier_lbl: UILabel!
    @IBOutlet weak var hide_view: UIView!
    @IBOutlet weak var tier_btn: UIButton!
    let nameArr = ["Tous","VIP","Support"]
    var is_contact = false
    var selectedarr: [Bool] =  [true,false,false,false,false]
    @IBOutlet weak var menuBttn: UIBarButtonItem!
    var trailingSwipe: [Bool] = []
    @IBOutlet weak var collectionVew: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
          menuBttn.action = #selector(menuButtonTapped(sender:))
        for i in 0 ..< 5 {
            trailingSwipe.append(false)
        }
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }
    @IBAction func edit_btn(_ sender: Any) {
       func_PUSH_VC("contact_photo_edit_VC")
    }
    
    @IBAction func add_contact(_ sender: Any) {
     //func_PUSH_VC("contact_photo_edit_VC")
        
        
    }
    
    
    @IBAction func option_btn(_ sender: Any) {
        is_contact = !is_contact
        tbl_contact_list.reloadData()
        
    }
    
    @IBAction func selected(_ sender: UIButton){
        if sender.isSelected{
            sender.isSelected = false
            print("selected")
        } else {
            sender.isSelected = true
            print("unselected")

        }
        tbl_contact_list.reloadData()
    }
    
    @objc func select_btn(_ sender: UIButton){
        let index = IndexPath(row: sender.tag, section: 0)
        if index.row == 1 {
            func_PUSH_VC("contact_Info_nopicture_VC")
        }else if index.row == 3{
              func_PUSH_VC("contact_Info_Picture_VC")
        }
      
        print("hello")
    }
    
    @IBAction func target_email_BTn(_ sender: Any) {
        
        func_PUSH_VC("Emailing_Option_VC")
    }
    
    
    @IBAction func buisness_btn(_ sender: Any) {
         func_PUSH_VC("Home_Page_VC")
        
    }
    
    
    @IBAction func urgence_btn(_ sender: Any) {
        func_PUSH_VC("Urgence_Home_Page_VC")
        
    }
    
    @objc func profile_edit_btn (_ sender:UIButton){
        func_PUSH_VC("contact_Info_Picture_VC")
    }
    
    
    
}





extension Contact_List_VC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 83
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath) as! contact_list_tableCell
        cell.check_box_btn.addTarget(self, action: #selector(selected(_:)), for: .touchUpInside)
        cell.check_box_btn.tag = indexPath.row
//         cell.leftSwipeViewWidth.constant = 0.0
        
        
        if indexPath.row == 3 {
            cell.userImg.image = UIImage(named: "3840x2160-2616959-new-york-city-computer-wallpaper-download")
        } else {
             cell.userImg.image = UIImage(named: "smith")
        }
        cell.select_BTN.addTarget(self, action:#selector(select_btn(_:)) , for: .touchUpInside)
        cell.select_BTN.tag = indexPath.row
        cell.profile_select.addTarget(self, action: #selector(profile_edit_btn(_:)), for: .touchUpInside)
        if is_contact {
            cell.view_width.constant = 40
            cell.profile_leading.constant = 5
            cell.check_box_btn.isHidden = false
            tier_img.isHidden = true
            setting_img.isHidden = true
//            tier_lbl.text = "tout sélectionner"
            tier_lbl.isHidden = true
            option_lbl.text = "annuler"
            hide_view.isHidden = false
            tier_btn.setTitle("tout sélectionner", for: .normal)
            
            
        } else {
            cell.view_width.constant = 0
            cell.check_box_btn.isHidden = true
            cell.profile_leading.constant = 20
            tier_lbl.text = "tier par"
            tier_img.isHidden = false
            setting_img.isHidden = false
            option_lbl.text = "options"
            hide_view.isHidden = true
            tier_btn.setTitle("", for: .normal)
            tier_lbl.isHidden = false

            

           }
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        
        
        for i in 0..<indexPath.row{
        
        if i == indexPath.row {
             selectedarr[i] = true
           } else {
            selectedarr[i] = false
          }
        }
        
        for i in 0..<5{
            if selectedarr[i] == true{
                cell.check_box_btn.setImage(UIImage(named: "checked"), for: .normal)
                } else{
              cell.check_box_btn.setImage(UIImage(named: "check_box"), for: .normal)
            }
        }
        
        
        
        
        
        return cell
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let call = UIContextualAction.init(style: .normal, title: "") { (action, view, nil) in
            print("contact")
        }
        call.image = UIImage(named: "phone")
         call.backgroundColor = hexStringToUIColor(hex: "#61AE18")
        
        let sms = UIContextualAction.init(style: .normal, title: "") { (action, view, nil) in
            print("sms")
        }
        
         sms.image = UIImage(named: "message")
        sms.backgroundColor = hexStringToUIColor(hex: "#808080")
        
        
        let email = UIContextualAction.init(style: .normal, title: "") { (action, view, nil) in
            print("email")
        }
        
         email.image = UIImage(named: "email")
         email.backgroundColor = hexStringToUIColor(hex: "#52A9F5")
        
        let calendar = UIContextualAction.init(style: .normal, title: "") { (action, view, nil) in
            print("calendar")
        }
         calendar.image = UIImage(named: "Tracé 234")
         calendar.backgroundColor = hexStringToUIColor(hex: "#808080")
        
        let confi =  UISwipeActionsConfiguration(actions: [call,sms,email,calendar])
        confi.performsFirstActionWithFullSwipe = false
        
        return confi
    }
//     func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
//        let delete = UITableViewRowAction(style: .default, title: "\u{267A}\n Delete") { action, index in
//            print("more button tapped")
//            self.tableView(tbl_contact_list, commitEditingStyle: UITableViewCell.EditingStyle.Delete, forRowAtIndexPath: indexPath)
//        }
//        delete.backgroundColor = UIColor(named: "#ef3340")
//
//        let apply = UITableViewRowAction(style: .default, title: "\u{2606}\n Like") { action, index in
//            print("favorite button tapped")
//            self.tableView(tbl_contact_list, commitEditingStyle: UITableViewCell.EditingStyle.Insert, forRowAtIndexPath: indexPath)
//        }
//        apply.backgroundColor = UIColor.orangeColor
//
//        let take = UITableViewRowAction(style: .normal, title: "\u{2605}\n Rate") { action, index in
//            print("share button tapped")
//           self.tableView?(tbl_contact_list, commit: UITableViewCell.EditingStyle.none, forRowAt: indexPath)
//         //   self.tableView(tbl_contact_list, commitEditingStyle: UITableViewCell.EditingStyle.None, forRowAtIndexPath: indexPath)
//        }
//        take.backgroundColor = UIColor(named: "#00ab84")
//
//        return [take, apply, delete]
//    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction.init(style: .normal, title: "") { (action, view, nil) in
            print("delete")
        }
        delete.image = #imageLiteral(resourceName: "Delete a Contact")
        delete.backgroundColor = UIColor.red
        let edit = UIContextualAction.init(style: .normal, title: "") { (action, view, nil) in
            print("edit")
        }
        
        edit.image = UIImage(named: "Eject From a group (Slided Contact)")
        edit.backgroundColor = UIColor.orange
        let confi =  UISwipeActionsConfiguration(actions: [delete,edit])
        confi.performsFirstActionWithFullSwipe = false
        
        return confi
        
    }
//    @available(iOS 11.0, *)
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let cell = tbl_contact_list.dequeueReusableCell(withIdentifier: "cell-3") as! contact_list_tableCell
//
//        let delete = UIContextualAction.init(style: .normal, title: "") { (action, view, nil) in
//            print("delete")
//            var frame = cell.leftSwipeView.frame
//            frame.origin.x = 0
//            frame.origin.y = 0
//            frame.size.height = 85
//            frame.size.width = 120
//
//            cell.leftSwipeView.frame = frame
//
//            view.addSubview(cell.leftSwipeView)
//        }
//
////        for i in 0..<trailingSwipe.count {
////            if i == indexPath.row {
////                if trailingSwipe[i] {
////                        trailingSwipe[i] = false
////                    cell.leftSwipeViewWidth.constant = 0
////
////                } else {
////                       trailingSwipe[i] = true
////                    cell.leftSwipeViewWidth.constant = 120.0
////
////                }
////
////            }
////        }
////
////        cell.leftSwipeView.bringSubviewToFront(self.view)
//
//       // cell.deleteBttn.setT, for: <#T##UIControl.State#>)
//
////         view.backgroundColor = .white
////     //   delete.title
////        delete.image = UIImage.init(named: "Call a contact")
//
//      //  leave.backgroundColor = .white
//
//
//        return UISwipeActionsConfiguration(actions: [delete])
//    }
//
//
////    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
////        let subViews = tableView.subviews.filter { (view) -> Bool in
////            if NSStringFromClass(view.classForCoder) == "UISwipeActionPullView" {
////                return true
////            }
////            return false
////        }
////        if subViews.count > 0 {
////            let bgView = subViews[0]
////            bgView.backgroundColor = .green
////
////        }
////    }
//    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
//
//        self.tbl_contact_list.subviews.forEach { subview in
//            print("YourTableViewController: \(String(describing: type(of: subview)))")
//            if (String(describing: type(of: subview)) == "UISwipeActionPullView") {
//                if (String(describing: type(of: subview.subviews[0])) == "UISwipeActionStandardButton") {
//                    var deleteBtnFrame = subview.subviews[0].frame
//                    deleteBtnFrame.origin.y = 0
//                    deleteBtnFrame.size.height = 85
//                    deleteBtnFrame.size.width = 120
//                    // Subview in this case is the whole edit View
////                    subview.frame.origin.y =  subview.frame.origin.y + 5
//                    subview.frame.size.height = 85
//                     subview.frame.size.width = 120
//                    subview.subviews[0].frame = deleteBtnFrame
//
//                    subview.backgroundColor = UIColor.white
//                }
//            }
//        }
//    }
    
}

extension Contact_List_VC: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nameArr.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contact_list_CollectionViewCell", for: indexPath) as! contact_list_CollectionViewCell
        //cell.nameLbl.text = nameArr[indexPath.row]
        cell.textButton.setTitle(nameArr[indexPath.item], for: .normal)
        if indexPath.item == 1 {
          cell.textButton.setTitleColor(UIColor.lightGray, for: .normal)
        } else if indexPath.item == 2 {
             cell.textButton.setTitleColor(UIColor.lightGray, for: .normal)
        }
        cell.textButton.addTarget(self, action: #selector(selectedBttn(_:)), for: .touchUpInside)
        cell.textButton.tag = indexPath.item
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contact_list_CollectionViewCell", for: indexPath) as! contact_list_CollectionViewCell
        if indexPath.row == 1 {
            targetBttn.setImage(UIImage(named: "greenbtn"), for: .normal)
            //cell.nameLbl.textColor = hexStringToUIColor(hex: "#5F912F")
            
        //targetBttn.imageView?.image = UIImage(named: "Group 87-2")
        }
        else {
            // targetBttn.setImage(UIImage(named: "Group 87"), for: .normal)
        }
        
        
    }
    @objc func selectedBttn(_ sender: UIButton) {
        var convertedPoint : CGPoint = sender.convert(CGPoint.zero, to: self.collectionVew)
        var indexPath = self.collectionVew.indexPathForItem(at: convertedPoint)
        let cell = self.collectionVew.cellForItem(at: indexPath!) as! contact_list_CollectionViewCell
        cell.textButton.setTitleColor(hexStringToUIColor(hex: "#5F912F"), for: .normal)
        targetBttn.setImage(UIImage(named: "greenbtn"), for: .normal)
        if indexPath?.item == 0 {
            targetBttn.setImage(UIImage(named: "Group 87"), for: .normal)
        }
        
    }
    
    
    
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}




