//
//  contact_Info_Picture_VC.swift
//  BuzCard
//
//  Created by appentus on 8/11/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift

class contact_Info_Picture_VC: UIViewController,SideMenuControllerDelegate {

    @IBOutlet weak var coll_View: UICollectionView!
    
    @IBOutlet weak var menuBttn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
          menuBttn.action = #selector(menuButtonTapped(sender:))
        coll_View.delegate = self
        coll_View.dataSource = self
        
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: coll_View.frame.size.width, height: coll_View.frame.height)
        layout.scrollDirection = .horizontal
                layout.sectionInset = UIEdgeInsets (top: 0, left: 30, bottom: 0, right: 30)
        coll_View.collectionViewLayout = layout
        
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func topCalenderBtn(_ sender: UIButton) {
           func_PUSH_VC("PurposeInvitionVC")
    }
    @IBAction func shareInfoBtn(_ sender: UIButton) {
        func_PUSH_VC("share_Info_VC")
    }
    @IBAction func share_BTN(_ sender: Any) {
        func_PUSH_VC("share_Info_VC")
    }
    
    
    @IBAction func btn_modifier(_ sender: Any) {
        func_PUSH_VC("contact_Edit_VC")
        
    }
    @IBAction func business_VC(_ sender: Any) {
        
        func_PUSH_VC("Home_Page_VC")
    }
    @IBAction func btn_urgence(_ sender: Any) {
        func_PUSH_VC("Urgence_Home_Page_VC")
    }
    
}


extension contact_Info_Picture_VC: UICollectionViewDelegate{
    
    
}

extension contact_Info_Picture_VC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contact_Edit_CollectionCell", for: indexPath) as! contact_Edit_CollectionCell
        if indexPath.item == 3 {
            cell.profile_ImgViwe.image = UIImage(named: "addProfile")
        }
        return cell
    }
}

extension contact_Info_Picture_VC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/2 - 10, height: 145)
    }
}
