//  ViewController.swift
//  BuzCard

//  Created by iOS-Appentus on 31/July/2019.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit

class Splash_VC: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            self.func_PUSH_VC("Login_VC")
        }
    }

    
    
}

