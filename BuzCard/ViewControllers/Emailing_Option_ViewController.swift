//
//  Emailing_Option_ViewController.swift
//  BuzCard
//
//  Created by appentus technologies pvt. ltd. on 8/3/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift
class Emailing_Option_VC: UIViewController,SideMenuControllerDelegate {
    
    var loading_PP_VC:Loading_PP_VC!
    
    @IBOutlet weak var tbl_emailing:UITableView!
    
    var arr_select_section = [Bool]()
      @IBOutlet weak var menuBttn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
         menuBttn.action = #selector(menuButtonTapped(sender:))
        for _ in 0...5 {
            arr_select_section.append(false)
        }
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }

    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func submitBttn(_ sender: UIButton) {
        let storyBoard = UIStoryboard  (name: "POP-UP", bundle: nil)
        let loading_PP_VC = storyBoard.instantiateViewController(withIdentifier: "MailPopUpVC") as? MailPopUpVC
        loading_PP_VC?.delegate = self
        self.addChild(loading_PP_VC!)
        self.view.addSubview(loading_PP_VC!.view)
    }
    
    func func_OK() {
        func_success()
    }
    
    func func_Annuler() {
        print("Func annuler is calling")
    }
    
    func func_success() {
        let storyBoard = UIStoryboard  (name: "POP-UP", bundle: nil)
        let loading_PP_VC = storyBoard.instantiateViewController(withIdentifier: "Success_PP_VC") as? Success_PP_VC
        
        str_title = "Un email d'invitation a été envoyé"
        
        loading_PP_VC?.delegate_Success = self
        self.addChild(loading_PP_VC!)
        self.view.addSubview(loading_PP_VC!.view)
        
        
        
//        let s                   = UIScreen.main.bounds.size
//        let pv                  = UIStoryboard(name: "POP-UP", bundle: nil).instantiateViewController(withIdentifier: "Success_PP_VC") as! Success_PP_VC
//        pv.delegate_Success = self
//        pv.view.frame           = CGRect(x:0, y:0, width: s.width - 40, height: 180)
//        pv.view.backgroundColor = .clear
//        popUpEffectType         = .flipDown //.zoomIn(default)/.zoomOut/.flipUp/.flipDown
//        presentPopUpViewController(pv)
    }
    
    func func_OK_Success() {
        func_PUSH_VC("EmailContactVC")
    }
    
    
}
    


extension Emailing_Option_VC : UITableViewDelegate, UITableViewDataSource ,Mailing_PP_Delegate ,SUCCESS_POPUP_Delegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section < 3 {
            return 44
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section < 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-1") as! Emailing_Header_TableViewCell
            
            cell.btn_select.tag = section
            cell.btn_select.addTarget(self, action: #selector(btn_select_header_emailing(_:)), for: .touchUpInside)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2") as! Emailing_Group_Header_TableViewCell
            
            cell.btn_select.tag = section
            cell.btn_select.addTarget(self, action: #selector(btn_select_Group_header(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arr_select_section[section] {
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath)
        
        return cell
    }
    
    @IBAction func btn_select_header_emailing(_ sender:UIButton) {
        for i in 0..<arr_select_section.count {
            if i == sender.tag {
                if arr_select_section[i] {
                   arr_select_section[i] = false
                } else {
                    arr_select_section[i] = true
                }
            }
        }
        tbl_emailing.reloadData()
    }
    
    @IBAction func btn_select_Group_header(_ sender:UIButton) {
        for i in 0..<arr_select_section.count {
            if i == sender.tag {
                if arr_select_section[i] {
                    arr_select_section[i] = false
                } else {
                    arr_select_section[i] = true
                }
            }
        }
        tbl_emailing.reloadData()
    }
    
}
