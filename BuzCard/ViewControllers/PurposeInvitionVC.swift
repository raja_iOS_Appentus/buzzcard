//
//  PurposeInvitionVC.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/20/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift

class PurposeInvitionVC: UIViewController,UITextFieldDelegate ,SideMenuControllerDelegate{
 let myPickerData:[String] =  ["Data 1", "Data 2", "Data 3"]
    var nameArr = ["Chris","Titanic","Smith"]
    var tableFilterData: [String] = []
    
     @IBOutlet weak var backBttn: UIButton!
      @IBOutlet weak var menuBttn: UIBarButtonItem!
    
    @IBOutlet weak var debutBttn: UIButton!
    @IBOutlet weak var finBttn: UIButton!
    @IBOutlet weak var titileText: UITextField!
    @IBOutlet weak var lieuTextfielf: UITextField!
    @IBOutlet weak var showHideView: UIView!
     let thePicker = UIPickerView()
    @IBOutlet weak var openTableTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITableView!
    var isSearch: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
          thePicker.delegate = self
          thePicker.dataSource = self
        showHideView.isHidden = true
        openTableTextfield.delegate = self
        emailTextfield.isHidden = true
           menuBttn.action = #selector(menuButtonTapped(sender:))
        //   topTextField.text = "Saved"
        //  topTextField.inputView = thePicker
        // topTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//         emailTextfield.isHidden = false
//    }
    @IBAction func finBttn(_ sender: UIButton) {
    }
    @IBAction func debutBttn(_ sender: UIButton) {
    }
    @IBAction func backBttn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func openemailBttn(_ sender: UIButton) {
          showHideView.isHidden = false
        backBttn.setTitle("Retour", for: .normal)
        
    }
    @IBAction func inviteBttn(_ sender: UIButton) {
        func_PUSH_VC("ParticipateVC")
    }
    
    
    @IBAction func txt_Search(_ sender: UITextField) {
        tableFilterData = [String]()
         emailTextfield.isHidden = false
        for i in 0..<nameArr.count {
            let model = nameArr[i]
            let target = model
            if ((target as NSString?)?.range(of:openTableTextfield.text!, options: .caseInsensitive))?.location == NSNotFound
            { } else {
                tableFilterData.append(model)
            }
        }
        
        if (openTableTextfield.text! == "") {
            tableFilterData = nameArr
        }
        
      //  Model_Categories.shared.arr_category_list = arr_searched
        emailTextfield.reloadData()
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
//
//        var searchText  = textField.text! + string
//
//        if string  == "" {
//            searchText = (searchText as String).substring(to: searchText.index(before: searchText.endIndex))
//        }
//        if searchText == "" {
//            isSearch = false
//            emailTextfield.reloadData()
//        }
//        else{
//            if searchText.count > 2 {
//                getSearchArrayContains(searchText)
//            }
//        }
//        return true
//    }
//    func getSearchArrayContains(_ text : String) {
//
//        tableFilterData = nameArr.filter({$0.lowercased().contains(text)})
//        isSearch = true
//        emailTextfield.reloadData()
//    }
}
extension PurposeInvitionVC: UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableFilterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "email", for: indexPath) as! EmailCell
        cell.nameLbl.text = tableFilterData[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         showHideView.isHidden = true
        func_PUSH_VC("PropeserMapVC")
    }
}

extension PurposeInvitionVC: UIPickerViewDelegate, UIPickerViewDataSource {



    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return myPickerData.count
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return myPickerData[row]
    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // UITextField.text = myPickerData[row]
    }


}
