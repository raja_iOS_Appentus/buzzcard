//
//  rendezvous_VC.swift
//  BuzCard
//
//  Created by appentus on 8/12/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift

class rendezvous_VC: UIViewController,SideMenuControllerDelegate {

    
      @IBOutlet weak var menuBttn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
    
        menuBttn.action = #selector(menuButtonTapped(sender:))

        // Do any additional setup after loading the view.
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }
}
