//
//  popVC.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/26/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift

class popVC: UIViewController,SideMenuControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
    }
    

    @IBAction func okkBttn(_ sender: UIButton) {
         dismissPopUpViewController()
        NotificationCenter.default.post(name: Notification.Name("ok_actions"), object: nil, userInfo: ["tag":sender.tag])
     
    //  func_PUSH_VC("Home_Page_VC")
    }
    
    @IBAction func cancelBttn(_ sender: UIButton) {
        dismissPopUpViewController()
    }

}
