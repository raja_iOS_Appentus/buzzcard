//
//  contact_edit2_VC.swift
//  BuzCard
//
//  Created by appentus on 8/12/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift

class contact_edit2_VC: UIViewController,Loading_PP_Delegate,SideMenuControllerDelegate {
    func func_Annuler() {
              self.view.removeFromSuperview()
    }
    
    func func_OK() {
         func_PUSH_VC("Contact_List_VC")
    }
    
   
    
    @IBOutlet weak var menuBttn: UIBarButtonItem!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
          menuBttn.action = #selector(menuButtonTapped(sender:))
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }


    @IBAction func btn_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
}
    
    
    @IBAction func supprimerPopUpBttn(_ sender: UIButton) {
        let storyboard = UIStoryboard (name: "POP-UP", bundle: nil)
        let success_POPUP = storyboard.instantiateViewController(withIdentifier: "Loading_PP_VC") as! Loading_PP_VC
        self.addChild(success_POPUP)
        self.view.addSubview(success_POPUP.view)
        
        success_POPUP.delegate = self
    }
    
    @IBAction func centerSendBtn(_ sender: UIButton) {
        func_PUSH_VC("send_Info_VC")
    }
    
    @IBAction func btn_register(_ sender: Any) {
        push_VC(vc_name: "contact_Info_Picture_VC", storybord_name: "Main")
        
    }
    @IBAction func business_VC(_ sender: Any) {
        
        func_PUSH_VC("Home_Page_VC")
    }
    @IBAction func btn_urgence(_ sender: Any) {
        func_PUSH_VC("Urgence_Home_Page_VC")
    }
}
