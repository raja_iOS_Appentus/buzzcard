//
//  qr_code_mon_VC.swift
//  BuzCard
//
//  Created by appentus on 8/12/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift


class qr_code_mon_VC: UIViewController,SideMenuControllerDelegate {

    
    //  @IBOutlet weak var menuBttn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
    }
    
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        sideMenuController?.revealMenu()
        
    }
    @IBAction func bussinessBttn(_ sender: UIButton) {
        func_PUSH_VC("Home_Page_VC")
    }
    @IBAction func urgenceBttn(_ sender: UIButton) {
         func_PUSH_VC("Urgence_Home_Page_VC")
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
