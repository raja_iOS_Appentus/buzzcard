//
//  send_Info_VC.swift
//  BuzCard
//
//  Created by appentus on 8/11/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift

class send_Info_VC: UIViewController ,SideMenuControllerDelegate, SUCCESS_POPUP_Delegate{
    @IBOutlet weak var textLang: UITextField!

    @IBOutlet weak var hide_option_BTN: UIButton!
    @IBOutlet weak var view_one: UIView!
    @IBOutlet weak var view_two: UIView!
    
    
    let myPickerData:[String] =  ["Data 1", "Data 2", "Data 3"]
        let thePicker = UIPickerView()
   //  @IBOutlet weak var menuBttn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        textLang.inputView = thePicker
        view_one.isHidden = true
        thePicker.delegate = self
        thePicker.dataSource = self
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
      //   menuBttn.action = #selector(menuButtonTapped(sender:))
       
//    hide_option_BTN.setTitle("optional advancee", for: .normal)
        // Do any additional setup after loading the view.
    }
  
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        sideMenuController?.revealMenu()
        
    }
    @IBAction func bottomEnvoyerBtn(_ sender: UIButton) {
        func_PUSH_VC("share_Info_VC")
    }
    
    func func_OK_Success() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func hide_optopn_btn(_ sender: UIButton) {
        if sender.tag == 1{
            view_one.isHidden = true
            view_two.isHidden = false
        }else if sender.tag == 2{
            view_one.isHidden = false
            view_two.isHidden = true
        }
    }
    
    @IBAction func envoyerBttn(_ sender: UIButton) {
        let storyboard = UIStoryboard (name: "POP-UP", bundle: nil)
        let success_POPUP = storyboard.instantiateViewController(withIdentifier: "Success_PP_VC") as! Success_PP_VC
        self.addChild(success_POPUP)
        self.view.addSubview(success_POPUP.view)
        
        success_POPUP.delegate_Success = self
    }
    @IBAction func btn_annuler(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func buisness_btn(_ sender: Any) {
        func_PUSH_VC("Home_Page_VC")
        
    }
    
    
    @IBAction func urgence_btn(_ sender: Any) {
        func_PUSH_VC("Urgence_Home_Page_VC")
        
    }
}
extension send_Info_VC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return myPickerData.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return myPickerData[row]
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         textLang.text = myPickerData[row]
    }
    
    
}
