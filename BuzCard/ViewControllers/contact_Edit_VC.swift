//
//  contact_Edit_VC.swift
//  BuzCard
//
//  Created by appentus on 8/11/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift
class contact_Edit_VC: UIViewController,SideMenuControllerDelegate,Loading_PP_Delegate {

    @IBOutlet weak var coll_View: UICollectionView!
    @IBOutlet weak var firstname_TF: UITextField!
    @IBOutlet weak var name_TF: UITextField!
    @IBOutlet weak var email_TF: UITextField!
    @IBOutlet weak var email_TF2: UITextField!
    @IBOutlet weak var fisherman_TF: UITextField!
    @IBOutlet weak var select_group_TF: UITextField!
    @IBOutlet weak var delete_card_BTn: UILabel!
    @IBOutlet weak var business_BTN: UIButton!
    @IBOutlet weak var urgence_Btn: UIButton!
     @IBOutlet weak var menuBttn: UIBarButtonItem!
    var loading_PP_VC:Loading_PP_VC!
    func func_OK() {
        func_PUSH_VC("Contact_List_VC")
    }
    
    func func_Annuler() {
        print("Func annuler is calling")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
          menuBttn.action = #selector(menuButtonTapped(sender:))
        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: coll_View.frame.size.width, height: coll_View.frame.height)
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets (top: 0, left: 30, bottom: 0, right: 30)
        coll_View.collectionViewLayout = layout
}

    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }
    
    @IBAction func urgence(_ sender: Any) {
          func_PUSH_VC("Urgence_Home_Page_VC")
    }
    
    @IBAction func buisness(_ sender: Any) {
         func_PUSH_VC("Home_Page_VC")
    }
    
    
    @IBAction func delete_Card(_ sender: Any) {
        let storyBoard = UIStoryboard  (name: "POP-UP", bundle: nil)
                    loading_PP_VC = storyBoard.instantiateViewController(withIdentifier: "Loading_PP_VC") as? Loading_PP_VC
                    loading_PP_VC.delegate = self
                    self.addChild(loading_PP_VC)
                self.view.addSubview(loading_PP_VC.view)
    }
    
    @IBAction func btn_annular(_ sender: Any) {
self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btn_register(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
}

extension contact_Edit_VC: UICollectionViewDelegate{
    
}

extension contact_Edit_VC: UICollectionViewDataSource{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contact_Edit_CollectionCell", for: indexPath) as! contact_Edit_CollectionCell
        
        return cell
    }
}

extension contact_Edit_VC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/2 - 10, height: 145)
    }
    
}



