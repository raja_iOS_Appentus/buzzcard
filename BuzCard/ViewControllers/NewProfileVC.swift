//
//  NewProfileVC.swift
//  BuzCard
//
//  Created by appentus technologies pvt. ltd. on 8/26/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift

class NewProfileVC: UIViewController,SideMenuControllerDelegate {

    @IBOutlet weak var annularBttn: UIButton!
    @IBOutlet weak var enRegisterBttn: UIButton!
    @IBOutlet weak var modifierBttn: UIButton!
    @IBOutlet weak var barCodeImg: UIImageView!
     @IBOutlet weak var donnesFirstView: UIView!
      @IBOutlet weak var pourBottomView: UIView!
    @IBOutlet weak var donnesExpandBttn: UIButton!
    @IBOutlet weak var pourExpandBttn: UIButton!
    @IBOutlet weak var pourTopConstant: NSLayoutConstraint!
    @IBOutlet weak var donnesViewHeight: NSLayoutConstraint!
    @IBOutlet weak var assureTextfield: UITextField!
    @IBOutlet weak var directionTextfield: UITextField!
    @IBOutlet weak var mutulleTextfield: UITextField!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var surnameTextfield: UITextField!
    @IBOutlet weak var doBTextfield: UITextField!
    @IBOutlet weak var prenomTextfield: UITextField!
    @IBOutlet weak var mobilefield: UITextField!
        @IBOutlet weak var menuBttn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
annularBttn.isHidden = true
    enRegisterBttn.isHidden = true
          donnesFirstView.isHidden = true
        pourBottomView.isHidden = false
        pourTopConstant.constant = 10.0
         donnesExpandBttn.setImage(UIImage(named: "Open Down Contact-Profile Edit-Info"), for: .normal)
         pourExpandBttn.setImage(UIImage(named: "Close Up Contact-Profile Edit-Info"), for: .normal)
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
        menuBttn.action = #selector(menuButtonTapped(sender:))
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }
     @IBAction func enRegisterBttn(_ sender: UIButton) {
       assureTextfield.isUserInteractionEnabled = false
           directionTextfield.isUserInteractionEnabled = false
           mutulleTextfield.isUserInteractionEnabled = false
        
        nameTextfield.isUserInteractionEnabled = false
        surnameTextfield.isUserInteractionEnabled = false
        doBTextfield.isUserInteractionEnabled = false
        prenomTextfield.isUserInteractionEnabled = false
        mobilefield.isUserInteractionEnabled = false
      
        annularBttn.isHidden = true
        enRegisterBttn.isHidden = true
        self.view.endEditing(true)
        modifierBttn.isHidden = false
           barCodeImg.isHidden = false

        
    }
    @IBAction func modifierBttn(_ sender: UIButton) {
        annularBttn.isHidden = false
        enRegisterBttn.isHidden = false
          modifierBttn.isHidden = true
        barCodeImg.isHidden = true
        assureTextfield.isUserInteractionEnabled = true
        directionTextfield.isUserInteractionEnabled = true
        mutulleTextfield.isUserInteractionEnabled = true
        
        nameTextfield.isUserInteractionEnabled = true
        surnameTextfield.isUserInteractionEnabled = true
        doBTextfield.isUserInteractionEnabled = true
        prenomTextfield.isUserInteractionEnabled = true
        mobilefield.isUserInteractionEnabled = true
        
        
    }
    @IBAction func donnesExpandBigBttn(_ sender: UIButton) {
        donnesFirstView.isHidden = false
        pourBottomView.isHidden = true
        
        donnesExpandBttn.setImage(UIImage(named: "Close Up Contact-Profile Edit-Info"), for: .normal)
        pourExpandBttn.setImage(UIImage(named: "Open Down Contact-Profile Edit-Info"), for: .normal)
        pourTopConstant.constant = 260.0
        
    }
    @IBAction func pourExpandBigBttn(_ sender: UIButton) {
        donnesFirstView.isHidden = true
        pourBottomView.isHidden = false
        donnesExpandBttn.setImage(UIImage(named: "Open Down Contact-Profile Edit-Info"), for: .normal)
        pourExpandBttn.setImage(UIImage(named: "Close Up Contact-Profile Edit-Info"), for: .normal)
        pourTopConstant.constant = 10.0
    }
    
     @IBAction func annularBttn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
     }
    @IBAction func business_VC(_ sender: Any) {
        
        func_PUSH_VC("Home_Page_VC")
    }
    @IBAction func btn_urgence(_ sender: Any) {
        func_PUSH_VC("Urgence_Home_Page_VC")
    }

}
