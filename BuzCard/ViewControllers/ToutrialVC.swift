//
//  ToutrialVC.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/20/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift


class ToutrialVC: UIViewController ,SideMenuControllerDelegate{

  //  @IBOutlet weak var pageControlTop: DefaultPageControl!
    @IBOutlet weak var pageContoltop: DefaultPageControl!
      @IBOutlet weak var pageContolbottom: DefaultPageControl!
    @IBOutlet weak var collToutrial: UICollectionView!
    @IBOutlet weak var bottomCollectionView: UICollectionView!
   // @IBOutlet weak var menuBttn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
         //  menuBttn.action = #selector(menuButtonTapped(sender:))
        pageContoltop.pageIndicatorTintColor = UIColor.white
        pageContoltop.currentPageIndicatorTintColor =  UIColor(red: 95.0/255.0, green: 145.0/255.0, blue: 47.0/255.0, alpha: 1.0)
        pageContolbottom.pageIndicatorTintColor = UIColor.white
        pageContolbottom.currentPageIndicatorTintColor =  UIColor(red: 95.0/255.0, green: 145.0/255.0, blue: 47.0/255.0, alpha: 1.0)
       // pageContoltop.transform = CGAffineTransform(scaleX: 2, y: 2)
       // pageContoltop.currentPage
        
    }
    
    
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        sideMenuController?.revealMenu()
        
    }
    @IBAction func bussinesBttn(_ sender: UIButton) {
        func_PUSH_VC("Home_Page_VC")
    }
    @IBAction func urgenceBttn(_ sender: UIButton) {
        func_PUSH_VC("Urgence_Home_Page_VC")
    }
}

extension ToutrialVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collToutrial {
            return 2
        } else {
            return 2
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collToutrial {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ToutrialCell
        
        
        return cell
        } 
        else {
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ToutrialCell
              return cell
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let screenSize = collectionView.bounds
         if collectionView == collToutrial {
        return CGSize(width: screenSize.width , height: screenSize.height )
         } else {
             return CGSize(width: screenSize.width , height: screenSize.height )
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let witdh = scrollView.frame.width - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x / witdh
        let roundedIndex = round(index)
          if scrollView == collToutrial {
            
             pageContoltop.currentPageIndicatorTintColor = UIColor(red: 95.0/255.0, green: 145.0/255.0, blue: 47.0/255.0, alpha: 1.0)
           //  pageContoltop.transform = CGAffineTransform(scaleX: 2.2, y: 2.2)
            self.pageContoltop?.currentPage = Int(roundedIndex)
          }else {
              pageContolbottom.currentPageIndicatorTintColor = UIColor(red: 95.0/255.0, green: 145.0/255.0, blue: 47.0/255.0, alpha: 1.0)
            //   pageContolbottom.transform = CGAffineTransform(scaleX: 2.2, y: 2.2)
            self.pageContolbottom?.currentPage = Int(roundedIndex)
        }
      
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btn_Left(_ sender: UIButton) {
        let collectionBounds = collToutrial.bounds
        let contentOffset = CGFloat(floor(collToutrial.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func btn_Right(_ sender: UIButton) {
        let collectionBounds = collToutrial.bounds
        let contentOffset = CGFloat(floor(collToutrial.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : collToutrial.contentOffset.y ,width : collToutrial.frame.width,height : collToutrial.frame.height)
        collToutrial.scrollRectToVisible(frame, animated: true)
    }
    
    @IBAction func btn_LeftBottom(_ sender: UIButton) {
        let collectionBounds = bottomCollectionView.bounds
        let contentOffset = CGFloat(floor(bottomCollectionView.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrameBottom(contentOffset: contentOffset)
    }
    
    @IBAction func btn_RightBottom(_ sender: UIButton) {
        let collectionBounds = bottomCollectionView.bounds
        let contentOffset = CGFloat(floor(bottomCollectionView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrameBottom(contentOffset: contentOffset)
    }
    
    func moveCollectionToFrameBottom(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : bottomCollectionView.contentOffset.y ,width : bottomCollectionView.frame.width,height : bottomCollectionView.frame.height)
           bottomCollectionView.scrollRectToVisible(frame, animated: true)
    }
}

