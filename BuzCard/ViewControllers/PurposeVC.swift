//
//  PurposeVC.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/20/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift

class PurposeVC: UIViewController,SideMenuControllerDelegate {

    let myPickerData:[String] =  ["Data 1", "Data 2", "Data 3"]
    @IBOutlet weak var debutBttn: UIButton!
    @IBOutlet weak var finBttn: UIButton!
    @IBOutlet weak var titileText: UITextField!
    @IBOutlet weak var lieuTextfielf: UITextField!
      @IBOutlet weak var menuBttn: UIBarButtonItem!
      let thePicker = UIPickerView()
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
           menuBttn.action = #selector(menuButtonTapped(sender:))
        // Do any additional setup after loading the view.
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }
    @IBAction func finBttn(_ sender: UIButton) {
        thePicker.delegate = self
        thePicker.dataSource = self
    }
    @IBAction func debutBttn(_ sender: UIButton) {
        thePicker.delegate = self
        thePicker.dataSource = self
    }
    @IBAction func backBttn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func inviteBttn(_ sender: UIButton) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PurposeVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return myPickerData.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return myPickerData[row]
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // UITextField.text = myPickerData[row]
    }
    
    
}
