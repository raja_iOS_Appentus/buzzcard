//
//  QRCode2VC.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/21/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift

class QRCode2VC: UIViewController,SideMenuControllerDelegate {

    
   //  @IBOutlet weak var menuBttn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
        
      //   menuBttn.action = #selector(menuButtonTapped(sender:))
        // Do any additional setup after loading the view.
    }
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        sideMenuController?.revealMenu()
       
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func topQrCodeBtn(_ sender: UIButton) {
    func_PUSH_VC("qr_code_mon_VC")
    }
    @IBAction func bottomQrCodeBtn(_ sender: UIButton) {
         func_PUSH_VC("qr_code_mon_VC")
    }
    @IBAction func bussinesBttn(_ sender: UIButton) {
         func_PUSH_VC("Home_Page_VC")
    }
    @IBAction func urgenceBttn(_ sender: UIButton) {
         func_PUSH_VC("Urgence_Home_Page_VC")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
