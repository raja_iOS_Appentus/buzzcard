//
//  EmailContactVC.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/19/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift
import IQKeyboardManagerSwift
class EmailContactVC: UIViewController,SideMenuControllerDelegate  {

     @IBOutlet weak var menuBttn: UIBarButtonItem!
    @IBOutlet weak var textViewStatic: UILabel!
    @IBOutlet weak var messageText: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        messageText.delegate = self
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
         menuBttn.action = #selector(menuButtonTapped(sender:))
        // Do any additional setup after loading the view.
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
        addDoneButtonOnKeyboard()
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }
   
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
    }
    @IBAction func back(_ sender: Any) {
        
      self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitBttn(_ sender: Any) {
        
        func_PUSH_VC("AssociateVC")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        messageText.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        messageText.resignFirstResponder()
        let bottomOffset = CGPoint(x: 0, y: 0)
        scrollView.setContentOffset(bottomOffset, animated: true)
    }
}
extension EmailContactVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewStatic.text = ""
        textViewStatic.isHidden = true
        let bottomOffset = CGPoint(x: 0, y: scrollView.bounds.size.height/2 - 45)
        scrollView.setContentOffset(bottomOffset, animated: true)
        //         self.scroll_view.frame.size = CGSize(width: UIScreen.main.bounds.width, height: 730.0)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textViewStatic.text == "" {
            textViewStatic.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."
    }

}
}
