//
//  contact_photo_edit_VC.swift
//  BuzCard
//
//  Created by appentus on 8/11/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift
class contact_photo_edit_VC: UIViewController,SideMenuControllerDelegate,Loading_PP_Delegate {
  @IBOutlet weak var menuBttn: UIBarButtonItem!
   
      var loading_PP_VC:Loading_PP_VC!
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = self.view.frame.size.width
        SideMenuController.preferences.basic.statusBarBehavior = .none
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        //        SideMenuController.preferences.basic.supportedOrientations = .all
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
          menuBttn.action = #selector(menuButtonTapped(sender:))
        // Do any additional setup after loading the view.
    }
    @IBAction func enRegisterBtn(_ sender: UIButton) {
         func_PUSH_VC("contact_Info_nopicture_VC")
    }
    @objc func menuButtonTapped(sender: UIBarButtonItem) {
        sideMenuController?.revealMenu()
        
    }
    @IBAction func supremierBtn(_ sender: UIButton) {
        let storyBoard = UIStoryboard  (name: "POP-UP", bundle: nil)
        loading_PP_VC = storyBoard.instantiateViewController(withIdentifier: "Loading_PP_VC") as? Loading_PP_VC
        loading_PP_VC.delegate = self
        self.addChild(loading_PP_VC)
        self.view.addSubview(loading_PP_VC.view)
    }
    func func_OK() {
        func_PUSH_VC("Contact_List_VC")
    }
    
    func func_Annuler() {
        print("Func annuler is calling")
    }
    
    @IBAction func back(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    @IBAction func business_VC(_ sender: Any) {
        
        func_PUSH_VC("Home_Page_VC")
    }
    @IBAction func btn_urgence(_ sender: Any) {
        func_PUSH_VC("Urgence_Home_Page_VC")
    }
}
