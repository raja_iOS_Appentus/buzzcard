//
//  HomeLoaderVC.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/26/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class HomeLoaderVC: UIViewController {

    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loader.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            self.loader.stopAnimating()
        }
    }
    
    

}
