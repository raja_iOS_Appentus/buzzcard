//
//  Home_Page_CollectionViewCell.swift
//  BuzCard
//
//  Created by appentus technologies pvt. ltd. on 8/3/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Home_Page_CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var lbl_email:UILabel!
    @IBOutlet weak var lbl_name:UILabel!
    @IBOutlet weak var btn_Profile:UIButton!
    
    @IBOutlet weak var editProfileNameBttn: UIButton!
}


