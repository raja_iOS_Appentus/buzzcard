//  CollectionViewCell.swift
//  BuzCard

//  Created by appentus technologies pvt. ltd. on 8/3/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit


protocol SUCCESS_POPUP_Delegate {
    func func_OK_Success()
}



protocol Loading_PP_Delegate {
    func func_OK()
    func func_Annuler()
}


protocol Mailing_PP_Delegate {
    func func_OK()
    func func_Annuler()
}

