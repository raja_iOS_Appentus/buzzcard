//  Loadf.swift
//  BuzCard

//  Created by appentus technologies pvt. ltd. on 8/3/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit


//let color_




class Loading_PP_VC: UIViewController {
    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var btn_OK:UIButton!
    @IBOutlet weak var btn_annuler:UIButton!
    
    var delegate:Loading_PP_Delegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lbl_title.text = "Chargement dolor sit amet, consectetur adipiscing elit."
    }
    
    @IBAction func btn_OK(_ sender:UIButton) {
        self.view.removeFromSuperview()
        delegate?.func_OK()
    }
    
    @IBAction func btn_annuler(_ sender:UIButton) {
       self.view.removeFromSuperview()
    }
    
    deinit {
        print("deinit")
    }
    
}
