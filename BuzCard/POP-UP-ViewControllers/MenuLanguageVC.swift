//
//  MenuLanguageVC.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/19/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class MenuLanguageVC: UIViewController {

    @IBOutlet weak var langCollection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let layout = UPCarouselFlowLayout()
        layout.itemSize = CGSize(width: langCollection.frame.size.width, height: langCollection.frame.height)
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets (top: 0, left: 30, bottom: 0, right: 30)
        langCollection.collectionViewLayout = layout
    }
   
    @IBAction func okkBttn(_ sender: UIButton) {
        dismissPopUpViewController()
    }
    
    @IBAction func cancelBttn(_ sender: UIButton) {
        dismissPopUpViewController()
    }
    @IBAction func leftBttn(_ sender: UIButton) {
//        let index = langCollection.indexPathsForVisibleItems.first
//        if index?.row != 0{
//            let sel = IndexPath(item: index!.row - 1, section: 0)
//            self.langCollection.scrollToItem(at: sel, at: .left, animated: true)
//
//        }
        
        let collectionBounds = langCollection.bounds
        let contentOffset = CGFloat(floor(langCollection.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    @IBAction func rightBttn(_ sender: UIButton) {
        
//        let index = langCollection.indexPathsForVisibleItems.first
//        if index?.row != 9{
//            let sel = IndexPath(item: index!.row + 1, section: 0)
//            self.langCollection.scrollToItem(at: sel, at: .right, animated: true)
//
//        }
        let collectionBounds = langCollection.bounds
        let contentOffset = CGFloat(floor(langCollection.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
      
    }
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : langCollection.contentOffset.y ,width : langCollection.frame.width,height : langCollection.frame.height)
        langCollection.scrollRectToVisible(frame, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MenuLanguageVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
            return 10
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "lang", for: indexPath) as! langCell
            
        
            return cell
     
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
  
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
            let screenSize = UIScreen.main.bounds
            return CGSize(width: collectionView.frame.width/3 , height: 80)
        }
    }
    

