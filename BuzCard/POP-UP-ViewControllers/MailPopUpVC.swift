//
//  MailPopUpVC.swift
//  BuzCard
//
//  Created by Appentus Technologies on 8/26/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class MailPopUpVC: UIViewController {
    var delegate:Mailing_PP_Delegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    

    @IBAction func btn_OK(_ sender:UIButton) {
        self.view.removeFromSuperview()
        delegate?.func_OK()
    }

    @IBAction func btn_annuler(_ sender:UIButton) {
        self.view.removeFromSuperview()

    }
    
}
