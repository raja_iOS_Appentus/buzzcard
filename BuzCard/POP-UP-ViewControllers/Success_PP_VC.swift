//  SUCCESS_VC.swift
//  BuzCard

//  Created by appentus technologies pvt. ltd. on 8/2/19.
//  Copyright © 2019 appentus. All rights reserved.



import UIKit

var str_title = ""

class Success_PP_VC: UIViewController {
    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var btn_OK:UIButton!
    
    var delegate_Success:SUCCESS_POPUP_Delegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lbl_title.text = str_title
    }
    
    @IBAction func btn_OK(_ sender:UIButton) {
        self.view.removeFromSuperview()
//        dismissPopUpViewController()/
        delegate_Success?.func_OK_Success()
    }
    
    
}
